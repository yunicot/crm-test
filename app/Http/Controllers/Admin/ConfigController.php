<?php

namespace App\Http\Controllers\Admin;

use App\Config;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConfigController extends Controller
{
    public function index()
    {
        $config_db = Config::get();
        $config = collect([]);

        $config_db->each(function ($item) use ($config){
            $name = $item->name;
            $config->$name = $item->value;
        });

        return view('admin.config.index', compact(['config']));
    }

    public function store(Request $request)
    {
        Config::updateOrCreate([
            'name' => 'columns'
        ],
        [
            'value' => $request->columns
        ]);
        Config::updateOrCreate([
            'name' => 'per_page'
        ],
        [
            'value' => $request->per_page
        ]);
        return redirect()->route('admin.config.index');
    }
}
