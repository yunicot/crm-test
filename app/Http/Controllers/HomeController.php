<?php

namespace App\Http\Controllers;

use App\Config;
use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $config_db = Config::get(['name', 'value']);
        $config = [];
        $config_db->each(function ($item) use (&$config){
            $config[$item->name] = $item->value;
        });

        $col = 12 / $config['columns'];

        $posts = Post::limit($config['per_page']);
        if($request->ajax())
        {
            $posts = $posts->skip($config['per_page'] * $request->page)->get();

            return response()->view('posts.many', compact(['posts', 'col']));
        }

        $posts = $posts->get();

        return view('home', compact(['posts', 'col']));
    }

    public function show($id)
    {
        $post = Post::findOrFail($id);

        $others = Post::whereCategoryId($post->category_id)->inRandomOrder()->limit(3)->get();

        return view('posts.show', compact(['post', 'others']));
    }
}
