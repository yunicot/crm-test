@extends('adminlte::page')

@section('content')
    <a href="{{ route('admin.posts.create') }}">
        <button class="btn btn-primary">Create</button>
    </a>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Category</th>
            <th>Tags</th>
            <th>Author</th>
        </tr>
        </thead>
        <tbody>
        @forelse($posts as $post)
            <tr>
                <td>{{ $post->id }}</td>
                <td>{{ $post->title }}</td>
                <td>{{ $post->category->title }}</td>
                <td>{{ $post->tags->pluck('title')->implode(', ') }}</td>
                <td>{{ $post->user->name }}</td>
            </tr>
        @empty
        <tr>
            <td colspan="5">Empty</td>
        </tr>
        @endforelse
        </tbody>
    </table>
@endsection