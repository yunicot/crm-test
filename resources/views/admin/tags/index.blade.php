@extends('adminlte::page')

@section('content')
    <a href="{{ route('admin.tags.create') }}">
        <button class="btn btn-primary">Create new</button>
    </a>
    <table class="table table-striped">
        <thead class="thead-light">
        <tr>
            <th>ID</th>
            <th>Title</th>
        </tr>
        </thead>
        <tbody>
        @forelse($tags as $tag)
            <tr>
                <td>{{ $tag->id }}</td>
                <td>{{ $tag->title }}</td>
            </tr>
        @empty
            <tr>
                <td colspan="2">Empty</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection