@extends('adminlte::page')

@section('content')
    <a href="{{ route('admin.categories.create') }}">
        <button class="btn btn-primary">Create New</button>
    </a>
    <table class="table table-striped">
        <thead class="thead-light">
        <tr>
            <td>ID</td>
            <td>Title</td>
        </tr>
        </thead>
        <tbody>
        @forelse($categories as $category)
            <tr>
                <td>{{ $category->id }}</td>
                <td>{{ $category->title }}</td>
            </tr>
        @empty
            <tr>
                <td colspan="2">Empty</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection