@extends('adminlte::page')

@section('content')
    <form action="{{ route('admin.config.store') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="columns">Columns</label>
            <select name="columns" id="columns" class="form-control">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="6">6</option>
                <option value="12">12</option>
            </select>
        </div>
        <div class="form-group">
            <label for="per_page">Per page</label>
            <input type="text" name="per_page" class="form-control" id="per_page" value="{{ $config->per_page }}">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@endsection
