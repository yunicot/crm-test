@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center" id="blog">
        @include('posts.many')
    </div>
    <div class="row justify-content-center">
        <button class="btn btn-primary" id="show_more">Show more...</button>
    </div>
</div>
@endsection

@push('js')
    <script>
    $(document).ready(function () {
        let page = 1;
        $('#show_more').on('click', function () {
            page++;
            axios.get('/?page=' + page)
                .then(res => {
                    if(res.data)
                        $('#blog').append(res.data);
                    else
                        $('#show_more').remove();
                })
                .catch(e => {
                    console.log(e);
                });
        });
    });
    </script>
@endpush
