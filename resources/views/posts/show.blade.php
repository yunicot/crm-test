@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">{{ $post->title }}</div>
                        <div class="card-title">{{ $post->user->name }}</div>
                        <div class="card-subtitle">{{ \Carbon\Carbon::parse($post->created_at)->format('d/m/Y H:i:s') }}</div>
                    </div>
                    <div class="card-body">
                        <div class="card-text">
                            {!!  $post->text !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
                @foreach($others as $other)
                <div class="col-md-4">
                    <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">{{ $other->title }}</h5>
                                <p class="card-text"></p>
                                <a href="{{ route('show', ['id' => $other->id]) }}">Show</a>
                            </div>
                        </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function () {

        });
    </script>
@endpush
