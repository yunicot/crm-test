@foreach($posts as $post)
    <div class="col-md-{{ $col }}">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">{{ $post->title }}</h5>
                <p class="card-text"></p>
                <a href="{{ route('show', ['id' => $post->id]) }}">Show</a>
            </div>
        </div>
    </div>
@endforeach