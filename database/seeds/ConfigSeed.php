<?php

use Illuminate\Database\Seeder;

class ConfigSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Config::create([
            'name' => 'columns',
            'value' => '3'
        ]);
        \App\Config::create([
            'name' => 'per_page',
            'value' => '9'
        ]);
    }
}
