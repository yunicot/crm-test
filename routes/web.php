<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/{id}', 'HomeController@show')->name('show')->where(['id' => '[0-9]+']);

Route::prefix('admin')->middleware('auth')->namespace('Admin')->name('admin.')->group(function (){
    Route::get('/', 'DashboardController@index');

    Route::get('/config', 'ConfigController@index')->name('config.index');
    Route::post('/config', 'ConfigController@store')->name('config.store');

    Route::resource('posts', 'PostController');
    Route::resource('tags', 'TagController');
    Route::resource('categories', 'CategoryController');
});
